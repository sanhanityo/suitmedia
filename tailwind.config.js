/** @type {import('tailwindcss').Config} */
export default {
  content: [
    "./components/**/*.{js,vue,ts}",
    "./layouts/**/*.vue",
    "./pages/**/*.vue",
    "./plugins/**/*.{js,ts}",
    "./app.vue",
    "./error.vue",
  ],
  theme: {
    colors: {
      white: {
        DEFAULT: "#fff",
      },
      primary: {
        DEFAULT: "#003459",
        80: "#002A48",
        60: "#00528C",
        40: "#0078CD",
      },
      secondary: {
        DEFAULT: "#F7DBA7",
        80: "#EEC77E",
        60: "#F1D092",
        40: "#FCEED5",
      },
      neutral: {
        DEFAULT: "#00171F",
        80: "#242B33",
        60: "#667479",
        40: "#99A2A5",
        20: "#CCD1D2",
        10: "#EBEEEF",
        0: "#FDFDFD",
      },
      error: {
        DEFAULT: "#FF564F",
      },
      success: {
        DEFAULT: "#34C759",
      },
      warning: {
        DEFAULT: "#FF912C",
      },
      info: {
        DEFAULT: "#00A7E7",
      },
    },
    container: {
      padding: {
        DEFAULT: "1rem",
        sm: "2rem",
        lg: "4rem",
        xl: "5rem",
        "2xl": "6rem",
      },
      screens: {
        xl: "1180px",
      },
    },
    extend: {
      fontFamily: {
        sans: ["GilRoy"],
      },
    },
  },
  plugins: [],
};
