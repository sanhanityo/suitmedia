export default defineEventHandler((event) => {
  return {
    seo: {
      title: "Homepage",
    },
    sections: [
      {
        id: 1,
        component: "HeroBanner",
        props: {
          config: null,
        },
      },
      {
        id: 2,
        component: "Pets",
        props: {
          config: null,
          data: {
            title: "Take A Look At Some Of Our Pets",
            subTitle: "Whats new?",
            items: [
              {
                image: "/images/pets/image-1.jpg",
                name: "MO231 - Pomeranian White",
                options: [
                  {
                    label: "Gene",
                    value: "Male",
                  },
                  {
                    label: "Age",
                    value: "02 Months",
                  },
                ],
                price: "6.900.000 VND",
              },
              {
                image: "/images/pets/image-2.jpg",
                name: "MO231 - Pomeranian White",
                options: [
                  {
                    label: "Gene",
                    value: "Male",
                  },
                  {
                    label: "Age",
                    value: "02 Months",
                  },
                ],
                price: "6.900.000 VND",
              },
              {
                image: "/images/pets/image-3.jpg",
                name: "MO231 - Pomeranian White",
                options: [
                  {
                    label: "Gene",
                    value: "Male",
                  },
                  {
                    label: "Age",
                    value: "02 Months",
                  },
                ],
                price: "6.900.000 VND",
              },
              {
                image: "/images/pets/image-4.jpg",
                name: "MO231 - Pomeranian White",
                options: [
                  {
                    label: "Gene",
                    value: "Male",
                  },
                  {
                    label: "Age",
                    value: "02 Months",
                  },
                ],
                price: "6.900.000 VND",
              },
              {
                image: "/images/pets/image-5.jpg",
                name: "MO231 - Pomeranian White",
                options: [
                  {
                    label: "Gene",
                    value: "Male",
                  },
                  {
                    label: "Age",
                    value: "02 Months",
                  },
                ],
                price: "6.900.000 VND",
              },
              {
                image: "/images/pets/image-6.jpg",
                name: "MO231 - Pomeranian White",
                options: [
                  {
                    label: "Gene",
                    value: "Male",
                  },
                  {
                    label: "Age",
                    value: "02 Months",
                  },
                ],
                price: "6.900.000 VND",
              },
              {
                image: "/images/pets/image-7.jpg",
                name: "MO231 - Pomeranian White",
                options: [
                  {
                    label: "Gene",
                    value: "Male",
                  },
                  {
                    label: "Age",
                    value: "02 Months",
                  },
                ],
                price: "6.900.000 VND",
              },
              {
                image: "/images/pets/image-8.jpg",
                name: "MO231 - Pomeranian White",
                options: [
                  {
                    label: "Gene",
                    value: "Male",
                  },
                  {
                    label: "Age",
                    value: "02 Months",
                  },
                ],
                price: "6.900.000 VND",
              },
            ],
          },
        },
      },
      {
        id: 3,
        component: "Banner1",
        props: {
          config: null,
          data: null,
        },
      },
      {
        id: 4,
        component: "Products",
        props: {
          config: null,
          data: {
            title: "Our Products",
            subTitle: "Hard to choose right products for your pets?",
            items: [
              {
                image: "/images/products/image-1.jpg",
                name: "MO231 - Pomeranian White",
                options: [
                  {
                    label: "Gene",
                    value: "Male",
                  },
                  {
                    label: "Age",
                    value: "02 Months",
                  },
                ],
                price: "6.900.000 VND",
                free: "Free Toy & Free Shaker",
              },
              {
                image: "/images/products/image-2.jpg",
                name: "MO231 - Pomeranian White",
                options: [
                  {
                    label: "Gene",
                    value: "Male",
                  },
                  {
                    label: "Age",
                    value: "02 Months",
                  },
                ],
                price: "6.900.000 VND",
              },
              {
                image: "/images/products/image-3.jpg",
                name: "MO231 - Pomeranian White",
                options: [
                  {
                    label: "Gene",
                    value: "Male",
                  },
                  {
                    label: "Age",
                    value: "02 Months",
                  },
                ],
                price: "6.900.000 VND",
              },
              {
                image: "/images/products/image-4.jpg",
                name: "MO231 - Pomeranian White",
                options: [
                  {
                    label: "Gene",
                    value: "Male",
                  },
                  {
                    label: "Age",
                    value: "02 Months",
                  },
                ],
                price: "6.900.000 VND",
              },
              {
                image: "/images/products/image-5.jpg",
                name: "MO231 - Pomeranian White",
                options: [
                  {
                    label: "Gene",
                    value: "Male",
                  },
                  {
                    label: "Age",
                    value: "02 Months",
                  },
                ],
                price: "6.900.000 VND",
              },
              {
                image: "/images/products/image-6.jpg",
                name: "MO231 - Pomeranian White",
                options: [
                  {
                    label: "Gene",
                    value: "Male",
                  },
                  {
                    label: "Age",
                    value: "02 Months",
                  },
                ],
                price: "6.900.000 VND",
              },
              {
                image: "/images/products/image-7.jpg",
                name: "MO231 - Pomeranian White",
                options: [
                  {
                    label: "Gene",
                    value: "Male",
                  },
                  {
                    label: "Age",
                    value: "02 Months",
                  },
                ],
                price: "6.900.000 VND",
              },
              {
                image: "/images/products/image-8.jpg",
                name: "MO231 - Pomeranian White",
                options: [
                  {
                    label: "Gene",
                    value: "Male",
                  },
                  {
                    label: "Age",
                    value: "02 Months",
                  },
                ],
                price: "6.900.000 VND",
              },
            ],
          },
        },
      },
      {
        id: 5,
        component: "Sellers",
        props: {
          config: null,
          data: null,
        },
      },
      {
        id: 6,
        component: "Banner2",
        props: {
          config: null,
          data: null,
        },
      },
      {
        id: 7,
        component: "News",
        props: {
          config: null,
          data: null,
        },
      },
    ],
  };
});
