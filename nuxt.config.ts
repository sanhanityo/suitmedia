// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  modules: ["@nuxt/fonts", "@nuxt/image"],
  devtools: {
    enabled: true,

    timeline: {
      enabled: true,
    },
  },
  css: ["~/assets/css/main.css"],
  postcss: {
    plugins: {
      tailwindcss: {},
      autoprefixer: {},
    },
  },
  fonts: {
    families: [
      {
        name: "Gilroy",
        global: true,
        src: "/font/Gilroy-Thin.woff2",
        weight: 100,
      },
      {
        name: "Gilroy",
        global: true,
        src: "/font/Gilroy-Light.woff2",
        weight: 300,
      },
      {
        name: "Gilroy",
        global: true,
        src: "/font/Gilroy-Regular.woff2",
        weight: 400,
      },
      {
        name: "Gilroy",
        global: true,
        src: "/font/Gilroy-Medium.woff2",
        weight: 500,
      },
      {
        name: "Gilroy",
        global: true,
        src: "/font/Gilroy-Semibold.woff2",
        weight: 600,
      },
      {
        name: "Gilroy",
        global: true,
        src: "/font/Gilroy-Extrabold.woff2",
        weight: 800,
      },
      {
        name: "Gilroy",
        global: true,
        src: "/font/Gilroy-Bold.woff2",
        weight: 700,
      },
      {
        name: "Gilroy",
        global: true,
        src: "/font/Gilroy-Black.woff2",
        weight: 900,
      },
    ],
    defaults: {
      weights: [400],
      styles: ["normal", "italic"],
    },
  },
});